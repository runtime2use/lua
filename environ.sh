#!/bin/bash

export LUA_PATH="$LANG_PATH/dist;;"
export LUA_CPATH="./?.so;/usr/local/lib/lua/5.3/?.so;/usr/local/share/lua/5.3/?.so;$LANG_PATH/libs/?.so"

################################################################################

ronin_require_lua () {
    echo hello world >/dev/null
}

ronin_include_lua () {
    motd_text "    -> Lua     : "$LUA_PATH
}

################################################################################

ronin_setup_lua () {
    echo hello world >/dev/null
}

